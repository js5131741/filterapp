const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLSchema,
  GraphQLInputObjectType,
} = require("graphql");

const Product = require("../model/product");

const ProductType = new GraphQLObjectType({
  name: "Product",
  fields: () => ({
    id: { type: GraphQLID },
    brand: { type: GraphQLString },
    cpu: { type: GraphQLString },
    ram: { type: GraphQLInt },
    ssd: { type: GraphQLInt },
    hdd: { type: GraphQLInt },
    price: { type: GraphQLInt },
  }),
});

const FiltersCheckedInputType = new GraphQLInputObjectType({
  name: "FiltersCheckedInput",
  fields: {
    brand: { type: GraphQLString },
    cpu: { type: GraphQLString },
    ram: { type: GraphQLInt },
    ssd: { type: GraphQLInt },
    hdd: { type: GraphQLInt },
    price: { type: GraphQLString },
  },
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    products: {
      type: new GraphQLList(ProductType),
      args: {
        filtersChecked: { type: FiltersCheckedInputType },
      },
      resolve(parent, args) {
        const filter = {};
        const { filtersChecked } = args;
        if (filtersChecked.brand) filter.brand = filtersChecked.brand;
        if (filtersChecked.cpu) filter.cpu = filtersChecked.cpu;
        if (filtersChecked.ram) filter.ram = filtersChecked.ram;
        if (filtersChecked.ssd) filter.ssd = filtersChecked.ssd;
        if (filtersChecked.hdd) filter.hdd = filtersChecked.hdd;
        if (filtersChecked.price) {
          const [lower, upper] = filtersChecked.price.split("-");
          filter.price = { $gte: parseInt(lower), $lte: parseInt(upper) };
        }

        return Product.find(filter);
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
});
