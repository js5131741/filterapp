const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
  {
    brand: String,
    cpu: String,
    ram: Number,
    ssd: Number,
    hdd: Number,
    price: Number,
  },
  { collection: "Laptops" }
);

const Product = mongoose.model("Product", productSchema);

module.exports = Product;
