const fs = require('fs');

// Function to generate a random integer within a specified range
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Function to generate random laptop data
function generateLaptop() {
  const brands = ['Dell', 'HP', 'Lenovo', 'Asus', 'Acer'];
  const cpus = ['Intel Core i5', 'Intel Core i7', 'AMD Ryzen 5', 'AMD Ryzen 7'];
  const ramOptions = [4, 8, 16, 32];
  const ssdOptions = [128, 256, 512, 1024];
  const hddOptions = [512, 1024];
  const priceRange = [50000, 80000];

  const brand = brands[getRandomInt(0, brands.length - 1)];
  const cpu = cpus[getRandomInt(0, cpus.length - 1)];
  const ram = ramOptions[getRandomInt(0, ramOptions.length - 1)];
  const ssd = ssdOptions[getRandomInt(0, ssdOptions.length - 1)];
  const hdd = hddOptions[getRandomInt(0, hddOptions.length - 1)];
  const price = getRandomInt(priceRange[0], priceRange[1]);

  return {
    brand,
    cpu,
    ram,
    ssd,
    hdd,
    price,
  };
}

// Function to generate an array of random laptops
function generateLaptops(count) {
  const laptops = [];
  for (let i = 0; i < count; i++) {
    laptops.push(generateLaptop());
  }
  return laptops;
}

// Generate 10 random laptops
const laptops = generateLaptops(50);

// Write the laptops array to a JSON file
fs.writeFileSync('laptops.json', JSON.stringify(laptops, null, 2));

console.log('Laptops data has been generated and saved to laptops.json file.');
