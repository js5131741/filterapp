import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import React from "react";
import { FiltersChecked, FilterProps } from "../types";

export default function Filter({
  filtersChecked,
  setFiltersChecked,
}: FilterProps) {
  const brands = ["Dell", "HP", "Lenovo", "Asus", "Acer"];
  const cpus = ["Intel Core i5", "Intel Core i7", "AMD Ryzen 5", "AMD Ryzen 7"];
  const ramOptions = [4, 8, 16, 32];
  const ssdOptions = [128, 256, 512, 1024];
  const hddOptions = [512, 1024];
  const priceRange = ["50000-60000", "60000-70000", "70000-80000"];

  const handleClick = (
    property: keyof FiltersChecked,
    value: string | number
  ) => {
    setFiltersChecked((prevState) => {
      const newState = {
        ...prevState,
        [property]: prevState[property] === value ? "" : value,
      };
      sessionStorage.setItem("Filter", JSON.stringify(newState));
      return newState;
    });
  };

  const handleClearFilter = () => {
    setFiltersChecked({
      brand: "",
      cpu: "",
      ram: 0,
      ssd: 0,
      hdd: 0,
      price: "",
    });
    sessionStorage.clear();
  };
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <h3 style={{ textAlign: "center" }}>Filter Options</h3>
      <Accordion style={{ width: "300px", marginBottom: "20px" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          Brand
        </AccordionSummary>
        <AccordionDetails>
          {brands.map((brand) => (
            <div>
              <FormControlLabel
                key={brand}
                value={brand}
                control={<Checkbox checked={filtersChecked.brand === brand} />} // Use Checkbox as the control
                label={brand}
                onClick={() => handleClick("brand", brand)} // Pass the brand value directly
              />
            </div>
          ))}
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ width: "300px", marginBottom: "20px" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>CPU</AccordionSummary>
        <AccordionDetails>
          {cpus.map((cpu) => (
            <div>
              <FormControlLabel
                key={cpu}
                value={cpu}
                control={<Checkbox checked={filtersChecked.cpu === cpu} />} // Use Checkbox as the control
                label={cpu}
                onClick={() => handleClick("cpu", cpu)} // Pass the brand value directly
              />
            </div>
          ))}
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ width: "300px", marginBottom: "20px" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>RAM</AccordionSummary>
        <AccordionDetails>
          {ramOptions.map((ram) => (
            <div>
              <FormControlLabel
                key={ram}
                value={ram}
                control={<Checkbox checked={filtersChecked.ram === ram} />} // Use Checkbox as the control
                label={`${ram} GB`}
                onClick={() => handleClick("ram", ram)} // Pass the brand value directly
              />
            </div>
          ))}
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ width: "300px", marginBottom: "20px" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>SSD</AccordionSummary>
        <AccordionDetails>
          {ssdOptions.map((ssd) => (
            <div>
              <FormControlLabel
                key={ssd}
                value={ssd}
                control={<Checkbox checked={filtersChecked.ssd === ssd} />} // Use Checkbox as the control
                label={`${ssd} GB`}
                onClick={() => handleClick("ssd", ssd)} // Pass the brand value directly
              />
            </div>
          ))}
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ width: "300px", marginBottom: "20px" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>HDD</AccordionSummary>
        <AccordionDetails>
          {hddOptions.map((hdd) => (
            <div>
              <FormControlLabel
                key={hdd}
                value={hdd}
                control={<Checkbox checked={filtersChecked.hdd === hdd} />} // Use Checkbox as the control
                label={`${hdd} GB`}
                onClick={() => handleClick("hdd", hdd)} // Pass the brand value directly
              />
            </div>
          ))}
        </AccordionDetails>
      </Accordion>
      <Accordion style={{ width: "300px", marginBottom: "20px" }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          Price Range (Rs)
        </AccordionSummary>
        <AccordionDetails>
          {priceRange.map((range) => (
            <div>
              <FormControlLabel
                key={range}
                value={range}
                control={<Checkbox checked={filtersChecked.price === range} />} // Use Checkbox as the control
                label={range}
                onClick={() => handleClick("price", range)} // Pass the brand value directly
              />
            </div>
          ))}
        </AccordionDetails>
      </Accordion>

      <Button onClick={handleClearFilter} style={{ color: "red" }}>
        Clear Filter
      </Button>
    </div>
  );
}
