import React from "react";
import { FilterProps } from "../types";
import { gql, useQuery } from "@apollo/client";
import { toast } from "react-toastify";
import { FiltersChecked } from "../types";
import { Card, CardContent, Grid, Typography } from "@mui/material";

const GET_LAPTOPS = gql`
  query products($filtersChecked: FiltersCheckedInput) {
    products(filtersChecked: $filtersChecked) {
      brand
      cpu
      ram
      ssd
      hdd
      price
    }
  }
`;

export default function Items({
  filtersChecked,
  setFiltersChecked,
}: FilterProps) {
  const { error, data } = useQuery(GET_LAPTOPS, {
    variables: { filtersChecked: filtersChecked },
  });

  React.useEffect(() => {
    if (error) {
      toast.error("Error while fetching");
    }
    if (data) {
      console.log(data);
    }
    console.log(filtersChecked);
  }, [filtersChecked, data, error]);

  return (
    <Grid container spacing={5}>
      {data?.products.length > 0 ? (
        data.products.map((laptop: FiltersChecked) => (
          <Grid item>
            <Card
              sx={{
                m: 0,
                p: 0,
                width: "300px",
                "&:hover": {
                  transform: "scale(1.05)",
                },
              }}
            >
              <CardContent
                sx={{
                  p: 0,
                }}
              >
                <div style={{ padding: "16px" }}>
                  <Typography variant="h6">
                    <b>Brand : </b>
                    {laptop.brand}
                  </Typography>
                  <Typography variant="h6">
                    <b>CPU: </b>
                    {laptop.cpu}
                  </Typography>
                  <Typography variant="h6">
                    <b>SSD: </b>
                    {laptop.ssd} GB
                  </Typography>
                  <Typography variant="h6">
                    <b>HDD: </b>
                    {laptop.hdd}GB
                  </Typography>
                  <Typography variant="h6">
                    <b>RAM: </b>
                    {laptop.ram}GB
                  </Typography>
                  <Typography variant="h6">
                    <b>PRICE: </b>
                    {laptop.price}
                  </Typography>
                </div>
              </CardContent>{" "}
            </Card>
          </Grid>
        ))
      ) : (
        <div style={{ color: "red" }}>
          <h1>No Laptops to Display</h1>
        </div>
      )}
    </Grid>
  );
}
