import React from "react";
import { Typography } from "@mui/material";

export default function Header() {
  return (
    <Typography variant="h3" textAlign="center">
      Filter Project
    </Typography>
  );
}
