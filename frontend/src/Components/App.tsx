import React from "react";
import Filter from "./Filter";
import Items from "./Items";
import Header from "./Header";
import { FiltersChecked } from "../types";
import { ApolloProvider, ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
  uri: "http://localhost:5000/graphql",
  cache: new InMemoryCache(),
});
function App() {
  const filterDataString = sessionStorage.getItem("Filter");

  const defaultFilter = filterDataString
    ? JSON.parse(filterDataString)
    : {
        brand: "",
        cpu: "",
        ram: 0,
        ssd: 0,
        hdd: 0,
        price: "",
      };

  const [filtersChecked, setFiltersChecked] =
    React.useState<FiltersChecked>(defaultFilter);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <ApolloProvider client={client}>
        <Header />
        <div style={{ display: "flex", gap: "100px", padding: "48px" }}>
          <Filter
            filtersChecked={filtersChecked}
            setFiltersChecked={setFiltersChecked}
          />

          <Items
            filtersChecked={filtersChecked}
            setFiltersChecked={setFiltersChecked}
          />
        </div>
      </ApolloProvider>
    </div>
  );
}

export default App;
