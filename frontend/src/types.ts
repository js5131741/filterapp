export type FiltersChecked = {
  brand: string;
  cpu: string;
  ram: number;
  ssd: number;
  hdd: number;
  price: string;
};

export type FilterProps = {
  filtersChecked: FiltersChecked;
  setFiltersChecked: React.Dispatch<React.SetStateAction<FiltersChecked>>;
};
